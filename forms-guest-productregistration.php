        <!--// Form - Guest - Product Registration [ //-->
        <form method="POST" id="registration_form">

          <div>

            <div>

              <h3>Provide Partner Information</h3>
              <em>To better assist you, please take a moment to provide these brief details and register for <?php echo $product_title; ?>.</em><hr/>

            </div>

            <fieldset>

              <legend>Name</legend>
              <em>Providing your name allows us to personalize our communication with you.</em>
              <input name="partner_first_name" placeholder="First Name" type="text" />
              <input name="partner_last_name" placeholder="Last Name" type="text" />

            </fieldset>

            <fieldset>

              <legend>Phone</legend>
              <em>Providing your phone allows us to contact if needed.</em>
              <input name="partner_phone" placeholder="Primary Telephone" type="telephone" />

            </fieldset>

            <fieldset>

              <legend>Address</legend>

              <!--//
              <input name="partner_address_primary" placeholder="Street Address" type="text" />
              <input name="partner_address_secondary" placeholder="Street Address" type="text" />
              <input name="partner_address_city" placeholder="City/Town" type="text" />
              //-->
              <input name="partner_address_postalcode" placeholder="Postal Code" type="text" />

              <em>Provide your country.</em>
              <select name="partner_address_country">
                <option disabled selected>Choose country</option>
                <option value="1">United States</option>
                <option value="2">Country 2</option>
              </select>

            </fieldset>

            <fieldset>

              <legend>Email</legend>

              <em>Your email will be the primary method we use to communicate.</em>
              <input name="partner_email" placeholder="Email" type="email" />

            </fieldset>

            <fieldset>

              <legend>Opt-in</legend>
              <em>By checking this box you agree to registration <a href="<?php echo $product_website; ?>/terms" target="_blank">terms and conditions</a>.</em>
              <label for="partner_marketingoptin">Yes, <?php echo $product_title; ?> can contact you for news and special offers.</label>
              <input name="partner_marketingoptin" id="partner_marketingoptin" checked type="checkbox" />

            </fieldset>

          </div>

          <div>

            <div>

              <h3>Provide Product Information</h3>
              <em>To better assist you, please take a moment to provide these brief details and register for <?php echo $product_title; ?>.</em><hr/>

            </div>

            <fieldset>

              <legend>Serial Number</legend>

              <em>12 - 20 digit serial number found on your welcome card or the box of your product</em>
              <input name="product_serialnumber" id="product_serialnumber" placeholder="Serial Number" type="text" />

            </fieldset>

            <fieldset>

              <legend>What bike brand do you own?</legend>

              <em>Type of your <?php echo $product_title; ?> product</em>
              <input type="text" class="product_brand" id="product_brand" placeholder="Begin typing brand">

              <script>

                $(document).ready(function() {

                  $('input.product_brand').typeahead({

                    name: 'brand',
                    remote: 'app-ajax-load-bikebrands.php?query=%QUERY'

                  });

                })

              </script>

              <select name="brand" id="brand">

                <option disabled selected>Select brand...</option>
<!--//
                <option value="1">Brand 1</option>
                <option value="2">Brand 2</option>
                <option value="3">Brand 3</option>
//-->

<?php

  //
  require 'app.php';

  //
  $query = mysqli_query($db,"SELECT bikebrand_ID, bikebrand_name FROM enviolo_bikebrands ORDER BY bikebrand_name ASC");

  //
  while($row = mysqli_fetch_array($query)) {

    $options = "<option value='" . $row['bikebrand_ID'] . "'>" . $row['bikebrand_name'] . "</option>";
    echo $options;

  }

  mysqli_close($db);

?>

              </select>

            </fieldset>

            <fieldset>

              <legend>Date of Purchase</legend>

              <em>When did you purchase or receive your <?php echo $product_title; ?> product?</em>
              <input name="product_dateofpurchase" id="product_dateofpurchase" placeholder="Date of Purchase" type="text" />

            </fieldset>

            <fieldset>

              <legend>Purchased From</legend>

              <em>Which dealer, if available, was your product purchased from?</em>
              <input name="product_purchasedfrom_dealer" id="product_purchasedfrom_dealer" placeholder="Start typing dealer name..." type="text" />

              <select name="product_purchasedfrom_dealercountry" id="product_purchasedfrom_dealercountry">
                <option disabled selected>Choose Country</option>
                <option value="1">Country 1</option>
                <option value="2">Country 2</option>
              </select>

              <input name="product_purchasedfrom_dealercitytown" id="product_purchasedfrom_dealercitytown" placeholder="City/Town" type="text" />

            </fieldset>

            <fieldset>

              <legend>Additional Questions</legend>

              <em>When did you become aware of the <?php echo $product_title; ?> products?</em>
              <select name="product_awareness" id="product_awareness">
                <option disabled selected>Please choose</option>
                <option value="1">My dealer showed me</option>
                <option value="2">I was aware beforehand</option>
              </select>

            </fieldset>

            <fieldset>

              <legend>Product Registration Policy</legend>

              <em>&nbsp;</em>
              <label for="product_waranteedisclaimer">Filling out this form is not a guarantee of warranty. Warranty will be assessed based on age and condition of product, proof of purchase, and other factors listed in the full warranty.</label>
              <input name="product_waranteedisclaimer" id="product_waranteedisclaimer" type="checkbox" />

            </fieldset>

          </div>

          <fieldset>

            <legend>Submit</legend>

            <em>Submit your information.</em>
            <input name="product_submit" value="Submit" type="submit" />

          </fieldset>

        </form>
        <!--// ] Form - Guest - Product Registration //-->
