<?php

  // Objects
  $product_title = "enviolo";
  $product_purpose = "Product Registration";
  $product_current_year = date('Y');

  // Phrases
  $product_page_title = "{$product_title} {$product_purpose}";
  $product_domain = "{$product_title}.com";
  $product_website = "http://www.{$product_title}.com";
  $product_email = "support@{$product_domain}";

  // Copy
  $product_description = "
    Thank you for choosing {$product_title}'s Harmony automatic shifting product.
    To register your purchase, please complete the brief form below.
    All {$product_title} products are backed by our 2-year limited warranty.
    If applicable, registration will enroll your product in our extended warranty program.
    If you have questions or need additional information please email {$product_email}.
  ";
  $product_disclaimer = "
    PRIVILEGED AND CONFIDENTIAL COMMUNICATION This electronic transmission, and any documents attached hereto, may contain confidential and/or legally privileged information.
    The information is intended only for use by the recipient(s) named above.
    If you have received this electronic message in error, please notify the sender and delete the electronic message.
    This message is protected by copyright laws and any disclosure, copying, distribution, or use of the contents of information received in error is strictly prohibited.
  ";
  $product_copyright = "&copy; {$product_current_year} {$product_title}. All rights reserved.";

?>
