-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 19, 2017 at 07:03 PM
-- Server version: 5.7.17
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `enviolo`
--

-- --------------------------------------------------------

--
-- Table structure for table `enviolo_bikebrands`
--

CREATE TABLE `enviolo_bikebrands` (
  `ID` int(11) NOT NULL COMMENT 'Bikebrand identifier in database',
  `bikebrand_ID` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Bikebrand identifier in app',
  `bikebrand_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Bikebrand name',
  `time_started` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Bikebrand action initiated',
  `time_updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Bikebrand action updated',
  `time_finished` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Bikebrand action concluded',
  `active` int(1) NOT NULL DEFAULT '1' COMMENT 'Bikebrand active in app… (0) inactive (1) active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `enviolo_bikebrands`
--

INSERT INTO `enviolo_bikebrands` (`ID`, `bikebrand_ID`, `bikebrand_name`, `time_started`, `time_updated`, `time_finished`, `active`) VALUES
(1, 'oiugHjbe', '4Ever', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(2, '2kzsDOgZ', 'Aarios', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(3, 'TYlsRd13', 'Ansmann', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(4, 'YNd2V203', 'AVE', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(5, '9rbN8TvH', 'Azor Bike', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(6, '6L9Qg6BE', 'Basmann', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(7, 'QBakc6Zc', 'Batavus', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(8, 'rf9U0xyp', 'Bergamont', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(9, 'Hx3itEVX', 'B&#710;ttcher', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(10, 'tKtGMvek', 'Breezer', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(11, 'C4AUayH9', 'Butchers &amp; Bicycles', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(12, '1quwmIxb', 'Campus', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(13, 'PZGTHTnq', 'Centurion', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(14, '8vODPCPc', 'Contoura', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(15, 'HKuXSl5x', 'Corratec', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(16, 'UF10gMwr', 'Cresta', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(17, 'PwMbUgXO', 'Cube', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(18, 'BGerwzLd', 'Dahon', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(19, 'RqziGyyL', 'Diamant', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(20, 'JQqlsdbU', 'Dutch ID', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(21, 'Yaw6daJx', 'eBike Company', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(22, '19G8YDlz', 'EVELO', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(23, '4UVQ5dxj', 'Externum', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(24, 'bIQLv55R', 'Falter', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(25, 'sJnD6JBF', 'Feldmeier', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(26, 'BY9esOHu', 'Felt', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(27, 'OelPlHzK', 'Flyer', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(28, 'bhkQEPAi', 'Freecross', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(29, 'DWUzODPv', 'Gazelle', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(30, 'U6hyU2fh', 'Gepida', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(31, 'iOsctgut', 'Giant', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(32, 'LwTsFuzZ', 'gobaX', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(33, 'xdcwHAzo', 'G&#710;ricke', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(34, '7lkFPJrL', 'Grace', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(35, 'hsomgW8P', 'Gudereit', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(36, 'mnDZERgB', 'Hartje', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(37, 'xtnpQDIc', 'Hercules', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(38, 'oGMsrLLW', 'Jaccs', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(39, 'bjNd5yUy', 'Kalkhoff', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(40, 'WLRKCuFg', 'Katarga', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(41, 'zljJt0Df', 'Kettler', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(42, 'qRLNHTrC', 'Koga', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(43, 'nn1718Ug', 'Kristall', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(44, '4guFv8jp', 'KTM', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(45, 'UwHiDNbt', 'Leaos', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(46, 'Z2HOQ1Bi', 'Maxx', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(47, '1DyW1uni', 'Merida', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(48, '7N2zcHOT', 'Moustache', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(49, 'ls9nW9gR', 'Novara', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(50, 'cTPQxbHn', 'Organic Transit', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(51, 'hDtdf8Rr', 'Origin8', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(52, 'Px68AobK', 'Other', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(53, 'vzKVMKxr', 'Panther', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(54, '2bo7kBme', 'Patria', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(55, 'xeXGGabD', 'Pegasus', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(56, 'FzITWk97', 'Raleigh', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(57, 'exmPDits', 'Riese & M&#184;ller', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(58, 'Ne6EUkEO', 'Rose', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(59, 'G1tSQ7NL', 'Schachner', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(60, 'b0XCCx40', 'Siga', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(61, 'RpDpxRCj', 'Simpel', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(62, 'Z1wNeJ70', 'Sparta', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(63, 'vkE2jnNr', 'SRM', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(64, 'emoybt5C', 'Stevens', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(65, 'B3oGQ1uM', 'TDR', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(66, 'a7YJk6pl', 'Third Element', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(67, 'syNwuZck', 'Thompson', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(68, 'SwYlr3wK', 'Tour de Suisse', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(69, 'AeVSAT3G', 'Trek', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(70, 'feLRsMJp', 'Trenergy', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(71, 'ijXqlhNv', 'Union', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(72, '1fKk0hPs', 'Urban Arrow', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(73, 'AQIohmYD', 'Utopia', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(74, 'RNbIzHIf', 'Velobility', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(75, 'Btt42fZK', 'Velo de Ville', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(76, 'FlWMCJyG', 'Victoria', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(77, 'AEMmOtJK', 'Villiger', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(78, 'Kz76FLUS', 'Volt', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(79, 'dCnwRoy5', 'VSF Fahrrad', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1),
(80, 'tRYlkJnS', 'Zemo', '2017-12-09 13:23:38', '2017-12-09 13:23:38', '2017-12-09 13:23:38', 1);

-- --------------------------------------------------------

--
-- Table structure for table `enviolo_registrations`
--

CREATE TABLE `enviolo_registrations` (
  `ID` int(11) NOT NULL COMMENT 'Registration identifier in database',
  `registration_ID` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Registration identifier in app',
  `registration_partner_first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Registration partner first name',
  `registration_partner_last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Registration partner last name',
  `registration_partner_phone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Registration partner primary email address',
  `registration_partner_address_postalcode` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Registration partner postal code',
  `registration_partner_address_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Registration partner address address country',
  `registration_partner_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Registration country listed in country 2-digit country code',
  `registration_partner_marketingoptin` tinyint(1) DEFAULT NULL COMMENT 'Registration partner marketing opt-in flag (BOOLEAN)',
  `registration_product_serialnumber` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Registration product (12-20 digit) serial number found on or within collateral material of product purchase.',
  `registration_product_brand` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Registration product brand',
  `registration_product_dateofpurchase` datetime DEFAULT NULL COMMENT 'Registration product purchase date.',
  `registration_product_purchasedfrom_dealer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Registration product dealer chosen from a list of active dealers',
  `registration_product_purchasedfrom_dealercountry` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Registration product dealer country',
  `registration_product_purchasedfrom_dealercitytown` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Registration product dealer city or town',
  `registration_product_awareness` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Registration product awareness selection range',
  `registration_product_waranteedisclaimer` tinyint(1) DEFAULT NULL COMMENT 'Registration product flag to inform users of no guarantees of warantees',
  `time_started` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Registration action initiated',
  `time_updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Registration action updated',
  `time_finished` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Registration action concluded',
  `active` int(1) NOT NULL DEFAULT '1' COMMENT 'Registration active in app... (0) inactive (1) active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `enviolo_registrations`
--

INSERT INTO `enviolo_registrations` (`ID`, `registration_ID`, `registration_partner_first_name`, `registration_partner_last_name`, `registration_partner_phone`, `registration_partner_address_postalcode`, `registration_partner_address_country`, `registration_partner_email`, `registration_partner_marketingoptin`, `registration_product_serialnumber`, `registration_product_brand`, `registration_product_dateofpurchase`, `registration_product_purchasedfrom_dealer`, `registration_product_purchasedfrom_dealercountry`, `registration_product_purchasedfrom_dealercitytown`, `registration_product_awareness`, `registration_product_waranteedisclaimer`, `time_started`, `time_updated`, `time_finished`, `active`) VALUES
(1, 'De4Rasf6', 'Adolphus', 'Nolan', '4697590006', '11221', 'USA', 'qappture@gmail.com', 1, 'E4R56288GHF56QLL89SX', 'Kellogg', '2017-05-16 00:00:00', 'Qappture', 'USA', 'Dallas', '1', 1, '2017-12-06 13:15:16', '2017-12-06 13:15:16', '2017-12-06 13:15:16', 1),
(2, 'Ee5Rasf7', 'Carter', 'Page', '4697590006', '11221', 'USA', 'carterpage@hotmail.com', 1, 'E4R56288GHF56QLL89SJ', 'Angle', '2017-05-16 00:00:00', 'Reagan', 'USA', 'Dallas', '1', 1, '2017-12-06 13:25:08', '2017-12-06 13:25:08', '2017-12-06 13:25:08', 1),
(3, 'Fe6Rbsf8', 'Adolphus', 'Nolan', '4697590006', '11221', 'USA', 'qappture@gmail.com', 1, 'E4R56288GHF56QLL89SX', 'Kellogg', '2017-05-16 00:00:00', 'Qappture', 'USA', 'Dallas', '1', 1, '2017-12-06 13:25:09', '2017-12-06 13:25:09', '2017-12-06 13:25:09', 1),
(4, 'Ge7Rcsf9', 'Winston', 'Salem', '7187590006', '10010', 'USA', 'salemw@gmail.com', 1, 'E4R56288GHF56QLL89SJ', 'Rake', '2017-05-16 00:00:00', 'Salem Biking', 'USA', 'Dallas', '1', 1, '2017-12-06 13:25:14', '2017-12-06 13:25:14', '2017-12-06 13:25:14', 1),
(5, 'He8Rdsf0', 'Adolphus', 'Nolan', '4697590006', '11221', 'USA', 'qappture@gmail.com', 1, 'E4R56288GHF56QLL89SX', 'Kellogg', '2017-05-16 00:00:00', 'Qappture', 'USA', 'Dallas', '1', 1, '2017-12-06 13:25:15', '2017-12-06 13:25:15', '2017-12-06 13:25:15', 1),
(6, 'Ie9Resf1', 'Don', 'McGinnley', '9177590006', '11221', 'USA', 'mcginnleydon@gmail.com', 1, 'E4R56288GHF56QLL89SJ', 'Franken', '2017-05-16 00:00:00', 'DMcG Bikes', 'USA', 'Dallas', '1', 1, '2017-12-06 13:25:17', '2017-12-06 13:25:17', '2017-12-06 13:25:17', 1),
(7, 'Je0Rfsf2', 'Adolphus', 'Nolan', '4697590006', '11221', 'USA', 'qappture@gmail.com', 1, 'E4R56288GHF56QLL89SX', 'Kellogg', '2017-05-16 00:00:00', 'Qappture', 'USA', 'Dallas', '1', 1, '2017-12-06 13:25:18', '2017-12-06 13:25:18', '2017-12-06 13:25:18', 1),
(8, 'Ke1Rgsf3', 'Bradley', 'Cooper', '7182340006', '10010', 'USA', 'bcooper@gmail.com', 1, 'E4R56288GHF56QLL89SJ', 'Charlie', '2017-05-16 00:00:00', 'Cooper Racing', 'USA', 'New York', '1', 1, '2017-12-06 13:25:19', '2017-12-06 13:25:19', '2017-12-06 13:25:19', 1),
(9, 'Le2Rhsf4', 'Adolphus', 'Nolan', '4697590006', '11221', 'USA', 'qappture@gmail.com', 1, 'E4R56288GHF56QLL89SX', 'Kellogg', '2017-05-16 00:00:00', 'Qappture', 'USA', 'Dallas', '1', 1, '2017-12-06 13:25:21', '2017-12-06 13:25:21', '2017-12-06 13:25:21', 1),
(10, 'Me3Risf5', 'Bradley', 'Cooper', '7182340006', '10010', 'USA', 'bcooper@gmail.com', 1, 'E4R56288GHF56QLL89SJ', 'Kellogg', '2017-05-16 00:00:00', 'Cooper Racing', 'USA', 'New York', '1', 1, '2017-12-06 13:25:22', '2017-12-06 13:25:22', '2017-12-06 13:25:22', 1),
(11, 'Adolphus', 'Nolan', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-06 15:40:07', '2017-12-06 15:40:07', '2017-12-06 15:40:07', 1),
(12, 'c4de00cc', 'Jacob', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-06 15:41:22', '2017-12-06 15:41:22', '2017-12-06 15:41:22', 1),
(13, '983e8419', 'Jacob', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-06 15:41:36', '2017-12-06 15:41:36', '2017-12-06 15:41:36', 1),
(14, '3945c58c', 'Jacob', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-06 15:43:07', '2017-12-06 15:43:07', '2017-12-06 15:43:07', 1),
(15, '65ad502b', 'Richard', 'Durbin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-06 15:49:16', '2017-12-06 15:49:16', '2017-12-06 15:49:16', 1),
(16, '1be900d9', 'Adolphus', 'Nolan', '4697590006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-06 15:52:49', '2017-12-06 15:52:49', '2017-12-06 15:52:49', 1),
(21, '9de065de', 'Gill', 'Kyle', '7187590006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-06 15:53:50', '2017-12-06 15:53:50', '2017-12-06 15:53:50', 1),
(22, '006a3435', 'Al', 'Nolan', '7187590006', '11221', 'USA', 'adolphusnolan@gmail.com', 1, '3RF5TB7SDC7WF9RH', 'Kannon', '2017-12-06 13:25:17', 'Brooklyn Biking', 'USA', 'Brooklyn', '1', 1, '2017-12-06 16:16:31', '2017-12-06 16:16:31', '2017-12-06 16:16:31', 1),
(25, 'cf373900', 'Jacob', 'Soboroff', '7187590006', '11221', 'USA', 'adolphusnolan@gmail.com', 1, '3RF5TB7SDC7WF9RJ', 'Kannon', '2017-12-01 13:25:17', 'Brooklyn Biking', 'USA', 'Brooklyn', '1', 1, '2017-12-06 16:18:25', '2017-12-06 16:18:25', '2017-12-06 16:18:25', 1),
(30, 'cf373901', 'Tom', 'Brokaw', '7187590006', '11221', 'USA', 'adolphusnolan@gmail.com', 1, '3RF5TB7SDC7WF9RJ', 'Kannon', '2017-12-01 13:25:17', 'Brooklyn Biking', 'USA', 'Brooklyn', '1', 1, '2017-12-06 16:19:46', '2017-12-06 16:19:46', '2017-12-06 16:19:46', 1),
(31, 'cf373902', 'Tom', 'Brokaw', '7187590006', '11221', 'USA', 'adolphusnolan@gmail.com', 1, '3RF5TB7SDC7WF9RJ', 'Kannon', '2017-12-01 13:25:17', 'Brooklyn Biking', 'USA', 'Brooklyn', '1', 1, '2017-12-06 16:19:49', '2017-12-06 16:19:49', '2017-12-06 16:19:49', 1),
(32, 'cf37390Q', 'Tom', 'Brokannon', '7187590006', '11221', 'USA', 'adolphusnolan@gmail.com', 1, '3RF5TB7SDC7WF9RJ', 'Kannon', '2017-12-01 13:25:17', 'Brooklyn Biking', 'USA', 'Brooklyn', '1', 1, '2017-12-06 16:20:00', '2017-12-06 16:20:00', '2017-12-06 16:20:00', 1),
(33, '26156074', 'Adolphus', 'Soboroff', '4697590006', '11221', 'USA', 'adolphusnolan@gmail.com', 0, '34TH85DH23ADW12', '1', '2017-12-06 13:25:17', 'Brooklyn Biking', '1', 'Brooklyn', '1', 0, '2017-12-06 16:38:28', '2017-12-06 16:38:28', '2017-12-06 16:38:28', 1),
(34, '76317a62', 'Cynthia', 'Burton', '2143352894', '75215', 'USA', 'cynthia.burton@gmail.com', 0, 'Q4TH85DH23ADW12', '1', '2017-11-11 15:13:17', 'The Biking Co.', '1', 'Cleveland', '1', 0, '2017-12-06 16:48:24', '2017-12-06 16:48:24', '2017-12-06 16:48:24', 1),
(35, '2f412375', 'Michael', 'Allen', '5167324432', '11221', 'USA', 'michael.allen@gmail.com', 0, '3RF5TB7SDC7WF9RJ', '3', '2016-11-11 15:13:17', 'The Biking Co.', '1', 'Washington D.C.', '1', 0, '2017-12-07 09:35:09', '2017-12-07 09:35:09', '2017-12-07 09:35:09', 1),
(36, '82f0af5a', 'Mike', 'Flynn', '7187590006', '11221', 'USA', 'mike.flynn@gmail.com', 0, 'QRF5TB7SDC7WF9RK', '1', '2015-12-01 13:25:17', 'The Biking Co.', '1', 'Washington D.C.', '1', 0, '2017-12-07 11:44:46', '2017-12-07 11:44:46', '2017-12-07 11:44:46', 1),
(37, 'f290e7f0', 'Marissa', 'Tomei', '4697590006', '23242', 'USA', 'mtomei@gmail.com', 0, 'Q4TH85DH23ADW12', '2', '2015-12-01 13:25:17', 'The Biking Co.', '1', 'Cleveland', '2', NULL, '2017-12-07 11:45:58', '2017-12-07 11:45:58', '2017-12-07 11:45:58', 1),
(38, '04f58da7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-07 11:50:39', '2017-12-07 11:50:39', '2017-12-07 11:50:39', 1),
(39, '81923bd9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-07 11:50:50', '2017-12-07 11:50:50', '2017-12-07 11:50:50', 1),
(40, '77da33ed', 'Delesia', 'Flynn', '7187590006', '11221', 'USA', 'mike.flynn@gmail.com', 0, 'QRF5TB7SDC7WF9RK', '1', '2017-12-07 12:52:01', 'The Biking Co.', '1', 'Washington D.C.', '1', 0, '2017-12-07 11:52:01', '2017-12-07 11:52:01', '2017-12-07 11:52:01', 1),
(41, '4442cf9b', 'Gretchin', 'Carlson', '7187590006', '11221', 'USA', 'gretchin.carlson@gmail.com', 0, 'QRF5TB7SDC7WF9RK', '1', '2017-12-07 12:53:30', 'The Biking Co.', '1', 'Washington D.C.', '1', 0, '2017-12-07 11:53:30', '2017-12-07 11:53:30', '2017-12-07 11:53:30', 1),
(42, '839e9efe', 'Andrea', 'Mitchell', '7187590006', '11221', 'USA', 'gretchin.carlson@gmail.com', NULL, 'QRF5TB7SDC7WF9RK', '1', '2017-12-07 12:54:03', 'The Biking Co.', '1', 'Washington D.C.', '1', 0, '2017-12-07 11:54:03', '2017-12-07 11:54:03', '2017-12-07 11:54:03', 1),
(43, 'a748b21e', 'Chris', 'Koons', '7187590006', '11221', 'USA', 'gretchin.carlson@gmail.com', NULL, 'QRF5TB7SDC7WF9RK', '1', '2015-12-01 13:25:17', 'The Biking Co.', '1', 'Washington D.C.', '1', 0, '2017-12-07 12:08:01', '2017-12-07 12:08:01', '2017-12-07 12:08:01', 1),
(44, '7db7687f', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-07 16:43:32', '2017-12-07 16:43:32', '2017-12-07 16:43:32', 1),
(45, 'db0674d7', 'al', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-07 16:43:55', '2017-12-07 16:43:55', '2017-12-07 16:43:55', 1),
(46, '90961e04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-07 16:51:28', '2017-12-07 16:51:28', '2017-12-07 16:51:28', 1),
(47, '76184f20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-07 16:52:02', '2017-12-07 16:52:02', '2017-12-07 16:52:02', 1),
(48, 'ffb742c6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-07 16:52:15', '2017-12-07 16:52:15', '2017-12-07 16:52:15', 1),
(49, '8acaa8a0', 'Leah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-07 16:52:23', '2017-12-07 16:52:23', '2017-12-07 16:52:23', 1),
(50, 'ca9d3c97', 'al', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-07 17:08:25', '2017-12-07 17:08:25', '2017-12-07 17:08:25', 1),
(51, '815fc177', 'al', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-07 17:08:30', '2017-12-07 17:08:30', '2017-12-07 17:08:30', 1),
(52, 'bc39b102', 'al', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-07 17:08:34', '2017-12-07 17:08:34', '2017-12-07 17:08:34', 1),
(53, '22731c3d', 'al', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-07 17:08:37', '2017-12-07 17:08:37', '2017-12-07 17:08:37', 1),
(54, '47cd76de', 'Adolphus', 'Nolan', '4697590005', '75227', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-07 17:11:51', '2017-12-07 17:11:51', '2017-12-07 17:11:51', 1),
(55, '9f4294c3', 'Adolphus', 'Nolan', '4697590005', '75227', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-07 17:11:53', '2017-12-07 17:11:53', '2017-12-07 17:11:53', 1),
(56, '5414181a', 'Adolphus', 'Nolan', '4697590006', '75215', '1', 'adolphusnolan@gmail.com', 0, 'FR3454TH56MU', '2', '2012-07-17 16:43:00', 'Branding', '1', 'Dallas', '1', NULL, '2017-12-07 17:12:56', '2017-12-07 17:12:56', '2017-12-07 17:12:56', 1),
(57, 'b8b984c0', '', '', '', '', '', '', 0, '', '', '2017-12-06 13:25:17', '', '', '', '0', 0, '2017-12-07 17:37:22', '2017-12-07 17:37:22', '2017-12-07 17:37:22', 1),
(58, 'b8b984c1', 'Jesse', 'Carson', '', '', '', '', 0, '', '', '2017-12-06 13:25:17', '', '', '', '0', 0, '2017-12-07 17:37:32', '2017-12-07 17:37:32', '2017-12-07 17:37:32', 1),
(59, 'b9bef135', 'Marissa', 'Tomei', '4697590006', '23242', 'USA', 'mtomei@gmail.com', 0, 'Q4TH85DH23ADW12', '2', '2015-12-01 13:25:17', 'The Biking Co.', '1', 'Cleveland', '2', NULL, '2017-12-07 17:39:10', '2017-12-07 17:39:10', '2017-12-07 17:39:10', 1),
(60, '1b04e397', 'Charles', 'Tomei', '4697590006', '23242', 'USA', 'mtomei@gmail.com', 0, 'Q4TH85DH23ADW12', '2', '2015-12-01 13:25:17', 'The Biking Co.', '1', 'Cleveland', '2', NULL, '2017-12-07 17:39:27', '2017-12-07 17:39:27', '2017-12-07 17:39:27', 1),
(61, 'bc6e7529', 'Carlton', 'Banks', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-07 17:42:15', '2017-12-07 17:42:15', '2017-12-07 17:42:15', 1),
(62, '8e812acd', 'Cedric', 'Bankston', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-07 17:43:28', '2017-12-07 17:43:28', '2017-12-07 17:43:28', 1),
(63, '325f6bc2', 'Bradley', 'Cooper', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-07 17:44:02', '2017-12-07 17:44:02', '2017-12-07 17:44:02', 1),
(64, 'e1271a83', 'Bradley', 'Benjamin', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-07 17:44:19', '2017-12-07 17:44:19', '2017-12-07 17:44:19', 1),
(65, '3a319c28', 'Button', 'Benjamin', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, '2017-12-07 17:44:39', '2017-12-07 17:44:39', '2017-12-07 17:44:39', 1),
(66, 'd82bf344', 'Button', 'Drop', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, '2017-12-07 17:45:05', '2017-12-07 17:45:05', '2017-12-07 17:45:05', 1),
(67, 'cbf0f162', 'Adolphus', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-07 17:48:50', '2017-12-07 17:48:50', '2017-12-07 17:48:50', 1),
(68, '298a9bc2', 'Adolphus', 'Nolan', '7185644281', '75215', 'USA', 'adolphusnolan@gmail.com', 1, 'Q2WS34DVG561DJU', 'Granular', NULL, 'RetroActive Cycling', 'USA', 'Dallas', '2', NULL, '2017-12-07 17:52:12', '2017-12-07 17:52:12', '2017-12-07 17:52:12', 1),
(69, '496791b5', 'Adolphus', 'Nolan', '7185644281', '75215', 'USA', 'adolphusnolan@gmail.com', 1, 'Q2WS34DVG561DJU', 'Granular', '2017-12-01 13:25:17', 'RetroActive Cycling', 'USA', 'Dallas', '2', NULL, '2017-12-07 18:47:19', '2017-12-07 18:47:19', '2017-12-07 18:47:19', 1),
(70, '72cfdb01', 'Chris', 'Wallace', '7185644281', '75215', 'USA', 'adolphusnolan@gmail.com', 1, 'Q2WS34DVG561DJU', 'Granular', '2017-12-01 13:25:17', 'RetroActive Cycling', 'USA', 'Dallas', '2', NULL, '2017-12-07 18:47:55', '2017-12-07 18:47:55', '2017-12-07 18:47:55', 1),
(71, '9c92ce7a', 'Chris', 'Wallace', '7185644281', '75215', 'USA', 'adolphusnolan@gmail.com', 1, 'Q2WS34DVG561DJU', 'Granular', '2017-12-01 13:25:17', 'RetroActive Cycling', 'USA', 'Dallas', '2', NULL, '2017-12-07 18:48:13', '2017-12-07 18:48:13', '2017-12-07 18:48:13', 1),
(72, 'cdb19685', 'Leah', 'Nolan', '2148688866', '78613', 'USA', 'leahnolan@@qappture.com', 1, 'QL2WEES34DVG561DJUK', 'Harmony', '2017-12-01 13:25:17', 'Mahill Cycling', 'USA', 'Cedar Park', '2', NULL, '2017-12-07 18:49:54', '2017-12-07 18:49:54', '2017-12-07 18:49:54', 1),
(73, 'a9bf1f6e', 'Leah', 'Nolan', '2148688866', '78613', 'USA', 'leahnolan@@qappture.com', 1, 'QL2WEES34DVG561DJUK', 'Harmony', '2017-12-01 13:25:17', 'Mahill Cycling', 'USA', 'Cedar Park', '2', NULL, '2017-12-07 18:52:32', '2017-12-07 18:52:32', '2017-12-07 18:52:32', 1),
(74, 'dab6e4f7', 'Leah', 'Nolan', '2148688866', '78613', 'USA', 'leahnolan@@qappture.com', 1, 'QL2WEES34DVG561DJUK', 'Harmony', '2017-12-01 13:25:17', 'Mahill Cycling', 'USA', 'Cedar Park', '2', NULL, '2017-12-07 18:52:48', '2017-12-07 18:52:48', '2017-12-07 18:52:48', 1),
(75, '3807cdc3', 'Leah', 'Nolan', '2148688866', '78613', 'USA', 'leahnolan@@qappture.com', 1, 'QL2WEES34DVG561DJUK', 'Harmony', '2017-12-01 13:25:17', 'Mahill Cycling', 'USA', 'Cedar Park', '2', NULL, '2017-12-07 18:53:45', '2017-12-07 18:53:45', '2017-12-07 18:53:45', 1),
(76, '8a02e33e', 'Leah', 'Nolan', '2148688866', '78613', 'USA', 'leahnolan@@qappture.com', 1, 'QL2WEES34DVG561DJUK', 'Harmony', '2017-12-01 13:25:17', 'Mahill Cycling', 'USA', 'Cedar Park', '2', NULL, '2017-12-07 18:54:07', '2017-12-07 18:54:07', '2017-12-07 18:54:07', 1),
(77, 'c4e39a85', 'Leah', 'Nolan', '2148688866', '78613', 'USA', 'leahnolan@@qappture.com', 1, 'QL2WEES34DVG561DJUK', 'Harmony', '2017-12-01 13:25:17', 'Mahill Cycling', 'USA', 'Cedar Park', '2', NULL, '2017-12-07 18:54:22', '2017-12-07 18:54:22', '2017-12-07 18:54:22', 1),
(78, '78431e6f', 'Leah', 'Nolan', '2148688866', '78613', 'USA', 'leahnolan@@qappture.com', 1, 'QL2WEES34DVG561DJUK', 'Harmony', '2017-12-01 13:25:17', 'Mahill Cycling', 'USA', 'Cedar Park', '2', NULL, '2017-12-07 19:11:28', '2017-12-07 19:11:28', '2017-12-07 19:11:28', 1),
(79, '7674583f', 'Al', 'Nolan', '4697590006', '78613', 'USA', 'al@qappture.com', 1, 'QL2WEES34DVG561DJUK', 'Harmony', '2017-12-01 13:25:17', 'Mahill Cycling', 'USA', 'Cedar Park', '2', NULL, '2017-12-17 08:32:26', '2017-12-17 08:32:26', '2017-12-17 08:32:26', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `enviolo_bikebrands`
--
ALTER TABLE `enviolo_bikebrands`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `bikebrand_ID` (`bikebrand_ID`);

--
-- Indexes for table `enviolo_registrations`
--
ALTER TABLE `enviolo_registrations`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `registration_ID` (`registration_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `enviolo_bikebrands`
--
ALTER TABLE `enviolo_bikebrands`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Bikebrand identifier in database', AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `enviolo_registrations`
--
ALTER TABLE `enviolo_registrations`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Registration identifier in database', AUTO_INCREMENT=80;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
