$(document).ready(function() {

  jsonData();

  $('#registration_form').submit(function (e) {

    var jData = $('#registration_form');

    $.ajax({

      type: jData.attr('method'),
      url: "app-api-put.php",
      //url: "jData.attr('action')",
      data: jData.serialize(),
      success: function(response) {
        jsonData();
        console.log(response);

      }

    });

    $('#registration_summary').empty();

    e.preventDefault();

  });

  //
  function jsonData() {

    $('#registration_summary').empty();

    //
    $.ajax({

      //
      type: 'GET',
      url: 'app-api-get.php',
      success: function(response) {

        $.each(response, function(index) {

          console.log(response[index].registration_partner_first_name);

          $('#registration_summary').append(

            response[index].registration_partner_first_name +
            ' ' +
            response[index].registration_partner_last_name +
            ' - ' +
            response[index].registration_ID +
            '<br />'
          );

        });

        console.log(response);

      }

    });

  }

});
