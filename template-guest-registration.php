<?php

  //
  require 'app-guest-properties.php';
  //

?>

<!DOCTYPE html>

<html>

<?php

  //
  require 'head-global.php';

?>

  <body>

    <main>

<?php

  //
  require 'header-guest.php';

?>

<?php

  //
  require 'section-guest-productregistration.php';

?>

<?php

  //
  require 'footer-guest.php';

?>

    </main>

  </body>

</html>
