-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 07, 2017 at 04:05 PM
-- Server version: 5.7.17
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `enviolo`
--

-- --------------------------------------------------------

--
-- Table structure for table `enviolo_registrations`
--

CREATE TABLE `enviolo_registrations` (
  `ID` int(11) NOT NULL COMMENT 'Registration identifier in database',
  `registration_ID` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Registration identifier in app',
  `registration_partner_first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Registration partner first name',
  `registration_partner_last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Registration partner last name',
  `registration_partner_phone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Registration partner primary email address',
  `registration_partner_address_postalcode` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Registration partner postal code',
  `registration_partner_address_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Registration partner address address country',
  `registration_partner_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Registration country listed in country 2-digit country code',
  `registration_partner_marketingoptin` tinyint(1) DEFAULT NULL COMMENT 'Registration partner marketing opt-in flag (BOOLEAN)',
  `registration_product_serialnumber` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Registration product (12-20 digit) serial number found on or within collateral material of product purchase.',
  `registration_product_brand` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Registration product brand',
  `registration_product_dateofpurchase` datetime DEFAULT NULL COMMENT 'Registration product purchase date.',
  `registration_product_purchasedfrom_dealer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Registration product dealer chosen from a list of active dealers',
  `registration_product_purchasedfrom_dealercountry` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Registration product dealer country',
  `registration_product_purchasedfrom_dealercitytown` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Registration product dealer city or town',
  `registration_product_awareness` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Registration product awareness selection range',
  `registration_product_waranteedisclaimer` tinyint(1) DEFAULT NULL COMMENT 'Registration product flag to inform users of no guarantees of warantees',
  `time_started` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Registration action initiated',
  `time_updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Registration action updated',
  `time_finished` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Registration action concluded',
  `active` int(1) NOT NULL DEFAULT '1' COMMENT 'Registration active in app... (0) inactive (1) active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `enviolo_registrations`
--

INSERT INTO `enviolo_registrations` (`ID`, `registration_ID`, `registration_partner_first_name`, `registration_partner_last_name`, `registration_partner_phone`, `registration_partner_address_postalcode`, `registration_partner_address_country`, `registration_partner_email`, `registration_partner_marketingoptin`, `registration_product_serialnumber`, `registration_product_brand`, `registration_product_dateofpurchase`, `registration_product_purchasedfrom_dealer`, `registration_product_purchasedfrom_dealercountry`, `registration_product_purchasedfrom_dealercitytown`, `registration_product_awareness`, `registration_product_waranteedisclaimer`, `time_started`, `time_updated`, `time_finished`, `active`) VALUES
(1, 'De4Rasf6', 'Adolphus', 'Nolan', '4697590006', '11221', 'USA', 'qappture@gmail.com', 1, 'E4R56288GHF56QLL89SX', 'Kellogg', '2017-05-16 00:00:00', 'Qappture', 'USA', 'Dallas', '1', 1, '2017-12-06 13:15:16', '2017-12-06 13:15:16', '2017-12-06 13:15:16', 1),
(2, 'Ee5Rasf7', 'Carter', 'Page', '4697590006', '11221', 'USA', 'carterpage@hotmail.com', 1, 'E4R56288GHF56QLL89SJ', 'Angle', '2017-05-16 00:00:00', 'Reagan', 'USA', 'Dallas', '1', 1, '2017-12-06 13:25:08', '2017-12-06 13:25:08', '2017-12-06 13:25:08', 1),
(3, 'Fe6Rbsf8', 'Adolphus', 'Nolan', '4697590006', '11221', 'USA', 'qappture@gmail.com', 1, 'E4R56288GHF56QLL89SX', 'Kellogg', '2017-05-16 00:00:00', 'Qappture', 'USA', 'Dallas', '1', 1, '2017-12-06 13:25:09', '2017-12-06 13:25:09', '2017-12-06 13:25:09', 1),
(4, 'Ge7Rcsf9', 'Winston', 'Salem', '7187590006', '10010', 'USA', 'salemw@gmail.com', 1, 'E4R56288GHF56QLL89SJ', 'Rake', '2017-05-16 00:00:00', 'Salem Biking', 'USA', 'Dallas', '1', 1, '2017-12-06 13:25:14', '2017-12-06 13:25:14', '2017-12-06 13:25:14', 1),
(5, 'He8Rdsf0', 'Adolphus', 'Nolan', '4697590006', '11221', 'USA', 'qappture@gmail.com', 1, 'E4R56288GHF56QLL89SX', 'Kellogg', '2017-05-16 00:00:00', 'Qappture', 'USA', 'Dallas', '1', 1, '2017-12-06 13:25:15', '2017-12-06 13:25:15', '2017-12-06 13:25:15', 1),
(6, 'Ie9Resf1', 'Don', 'McGinnley', '9177590006', '11221', 'USA', 'mcginnleydon@gmail.com', 1, 'E4R56288GHF56QLL89SJ', 'Franken', '2017-05-16 00:00:00', 'DMcG Bikes', 'USA', 'Dallas', '1', 1, '2017-12-06 13:25:17', '2017-12-06 13:25:17', '2017-12-06 13:25:17', 1),
(7, 'Je0Rfsf2', 'Adolphus', 'Nolan', '4697590006', '11221', 'USA', 'qappture@gmail.com', 1, 'E4R56288GHF56QLL89SX', 'Kellogg', '2017-05-16 00:00:00', 'Qappture', 'USA', 'Dallas', '1', 1, '2017-12-06 13:25:18', '2017-12-06 13:25:18', '2017-12-06 13:25:18', 1),
(8, 'Ke1Rgsf3', 'Bradley', 'Cooper', '7182340006', '10010', 'USA', 'bcooper@gmail.com', 1, 'E4R56288GHF56QLL89SJ', 'Charlie', '2017-05-16 00:00:00', 'Cooper Racing', 'USA', 'New York', '1', 1, '2017-12-06 13:25:19', '2017-12-06 13:25:19', '2017-12-06 13:25:19', 1),
(9, 'Le2Rhsf4', 'Adolphus', 'Nolan', '4697590006', '11221', 'USA', 'qappture@gmail.com', 1, 'E4R56288GHF56QLL89SX', 'Kellogg', '2017-05-16 00:00:00', 'Qappture', 'USA', 'Dallas', '1', 1, '2017-12-06 13:25:21', '2017-12-06 13:25:21', '2017-12-06 13:25:21', 1),
(10, 'Me3Risf5', 'Bradley', 'Cooper', '7182340006', '10010', 'USA', 'bcooper@gmail.com', 1, 'E4R56288GHF56QLL89SJ', 'Kellogg', '2017-05-16 00:00:00', 'Cooper Racing', 'USA', 'New York', '1', 1, '2017-12-06 13:25:22', '2017-12-06 13:25:22', '2017-12-06 13:25:22', 1),
(11, 'Adolphus', 'Nolan', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-06 15:40:07', '2017-12-06 15:40:07', '2017-12-06 15:40:07', 1),
(12, 'c4de00cc', 'Jacob', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-06 15:41:22', '2017-12-06 15:41:22', '2017-12-06 15:41:22', 1),
(13, '983e8419', 'Jacob', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-06 15:41:36', '2017-12-06 15:41:36', '2017-12-06 15:41:36', 1),
(14, '3945c58c', 'Jacob', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-06 15:43:07', '2017-12-06 15:43:07', '2017-12-06 15:43:07', 1),
(15, '65ad502b', 'Richard', 'Durbin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-06 15:49:16', '2017-12-06 15:49:16', '2017-12-06 15:49:16', 1),
(16, '1be900d9', 'Adolphus', 'Nolan', '4697590006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-06 15:52:49', '2017-12-06 15:52:49', '2017-12-06 15:52:49', 1),
(21, '9de065de', 'Gill', 'Kyle', '7187590006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-06 15:53:50', '2017-12-06 15:53:50', '2017-12-06 15:53:50', 1),
(22, '006a3435', 'Al', 'Nolan', '7187590006', '11221', 'USA', 'adolphusnolan@gmail.com', 1, '3RF5TB7SDC7WF9RH', 'Kannon', '2017-12-06 13:25:17', 'Brooklyn Biking', 'USA', 'Brooklyn', '1', 1, '2017-12-06 16:16:31', '2017-12-06 16:16:31', '2017-12-06 16:16:31', 1),
(25, 'cf373900', 'Jacob', 'Soboroff', '7187590006', '11221', 'USA', 'adolphusnolan@gmail.com', 1, '3RF5TB7SDC7WF9RJ', 'Kannon', '2017-12-01 13:25:17', 'Brooklyn Biking', 'USA', 'Brooklyn', '1', 1, '2017-12-06 16:18:25', '2017-12-06 16:18:25', '2017-12-06 16:18:25', 1),
(30, 'cf373901', 'Tom', 'Brokaw', '7187590006', '11221', 'USA', 'adolphusnolan@gmail.com', 1, '3RF5TB7SDC7WF9RJ', 'Kannon', '2017-12-01 13:25:17', 'Brooklyn Biking', 'USA', 'Brooklyn', '1', 1, '2017-12-06 16:19:46', '2017-12-06 16:19:46', '2017-12-06 16:19:46', 1),
(31, 'cf373902', 'Tom', 'Brokaw', '7187590006', '11221', 'USA', 'adolphusnolan@gmail.com', 1, '3RF5TB7SDC7WF9RJ', 'Kannon', '2017-12-01 13:25:17', 'Brooklyn Biking', 'USA', 'Brooklyn', '1', 1, '2017-12-06 16:19:49', '2017-12-06 16:19:49', '2017-12-06 16:19:49', 1),
(32, 'cf37390Q', 'Tom', 'Brokannon', '7187590006', '11221', 'USA', 'adolphusnolan@gmail.com', 1, '3RF5TB7SDC7WF9RJ', 'Kannon', '2017-12-01 13:25:17', 'Brooklyn Biking', 'USA', 'Brooklyn', '1', 1, '2017-12-06 16:20:00', '2017-12-06 16:20:00', '2017-12-06 16:20:00', 1),
(33, '26156074', 'Adolphus', 'Soboroff', '4697590006', '11221', 'USA', 'adolphusnolan@gmail.com', 0, '34TH85DH23ADW12', '1', '2017-12-06 13:25:17', 'Brooklyn Biking', '1', 'Brooklyn', '1', 0, '2017-12-06 16:38:28', '2017-12-06 16:38:28', '2017-12-06 16:38:28', 1),
(34, '76317a62', 'Cynthia', 'Burton', '2143352894', '75215', 'USA', 'cynthia.burton@gmail.com', 0, 'Q4TH85DH23ADW12', '1', '2017-11-11 15:13:17', 'The Biking Co.', '1', 'Cleveland', '1', 0, '2017-12-06 16:48:24', '2017-12-06 16:48:24', '2017-12-06 16:48:24', 1),
(35, '2f412375', 'Michael', 'Allen', '5167324432', '11221', 'USA', 'michael.allen@gmail.com', 0, '3RF5TB7SDC7WF9RJ', '3', '2016-11-11 15:13:17', 'The Biking Co.', '1', 'Washington D.C.', '1', 0, '2017-12-07 09:35:09', '2017-12-07 09:35:09', '2017-12-07 09:35:09', 1),
(36, '82f0af5a', 'Mike', 'Flynn', '7187590006', '11221', 'USA', 'mike.flynn@gmail.com', 0, 'QRF5TB7SDC7WF9RK', '1', '2015-12-01 13:25:17', 'The Biking Co.', '1', 'Washington D.C.', '1', 0, '2017-12-07 11:44:46', '2017-12-07 11:44:46', '2017-12-07 11:44:46', 1),
(37, 'f290e7f0', 'Marissa', 'Tomei', '4697590006', '23242', 'USA', 'mtomei@gmail.com', 0, 'Q4TH85DH23ADW12', '2', '2015-12-01 13:25:17', 'The Biking Co.', '1', 'Cleveland', '2', NULL, '2017-12-07 11:45:58', '2017-12-07 11:45:58', '2017-12-07 11:45:58', 1),
(38, '04f58da7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-07 11:50:39', '2017-12-07 11:50:39', '2017-12-07 11:50:39', 1),
(39, '81923bd9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-07 11:50:50', '2017-12-07 11:50:50', '2017-12-07 11:50:50', 1),
(40, '77da33ed', 'Delesia', 'Flynn', '7187590006', '11221', 'USA', 'mike.flynn@gmail.com', 0, 'QRF5TB7SDC7WF9RK', '1', '2017-12-07 12:52:01', 'The Biking Co.', '1', 'Washington D.C.', '1', 0, '2017-12-07 11:52:01', '2017-12-07 11:52:01', '2017-12-07 11:52:01', 1),
(41, '4442cf9b', 'Gretchin', 'Carlson', '7187590006', '11221', 'USA', 'gretchin.carlson@gmail.com', 0, 'QRF5TB7SDC7WF9RK', '1', '2017-12-07 12:53:30', 'The Biking Co.', '1', 'Washington D.C.', '1', 0, '2017-12-07 11:53:30', '2017-12-07 11:53:30', '2017-12-07 11:53:30', 1),
(42, '839e9efe', 'Andrea', 'Mitchell', '7187590006', '11221', 'USA', 'gretchin.carlson@gmail.com', NULL, 'QRF5TB7SDC7WF9RK', '1', '2017-12-07 12:54:03', 'The Biking Co.', '1', 'Washington D.C.', '1', 0, '2017-12-07 11:54:03', '2017-12-07 11:54:03', '2017-12-07 11:54:03', 1),
(43, 'a748b21e', 'Chris', 'Koons', '7187590006', '11221', 'USA', 'gretchin.carlson@gmail.com', NULL, 'QRF5TB7SDC7WF9RK', '1', '2015-12-01 13:25:17', 'The Biking Co.', '1', 'Washington D.C.', '1', 0, '2017-12-07 12:08:01', '2017-12-07 12:08:01', '2017-12-07 12:08:01', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `enviolo_registrations`
--
ALTER TABLE `enviolo_registrations`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `registration_ID` (`registration_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `enviolo_registrations`
--
ALTER TABLE `enviolo_registrations`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Registration identifier in database', AUTO_INCREMENT=44;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
