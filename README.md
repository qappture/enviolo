# fbProductRegistration (Enviolo)

![Banner](https://bytebucket.org/qappture/fbproductregistration/raw/4c9cb0e524e3e959690a087d48fbbf975bfe5d04/%2B/fbProductRegistration.png?token=583a77e2fc4113eff5c864e4b7211642340b3d2d)

Registration Form and API for enviolo Product Registrations

* Landing Page
* Customer Registration
* Customer Product Registration
* Tokenized API for both inputs and outputs

# Website

[http://enviolo.qappture.com](https://documenter.getpostman.com/view/3209786/collection/7ELZAEr)

# API

[API Specification](https://documenter.getpostman.com/view/3209786/collection/7ELZAEr)

# Solution Architecture

## Solution Architecture - Get All Registrations
![GetAllRegistrations](http://enviolo.qappture.com/enviolo-ProductRegistration-API-GetAllRegistrations.jpg)

* [api/registrations](http://enviolo.qappture.com/api/registrations)
* [api/registrations/{id}](http://enviolo.qappture.com/api/registrations/e2f2346a)

## Solution Architecture - Submit Registrations
![Submit Registrations](http://enviolo.qappture.com/enviolo-ProductRegistration-API-PartnerRegisters.jpg)

* [api/registrations/new/{PARAMS}](http://enviolo.qappture.com/api/registrations)

## Solution Architecture - Helpdesk Update
![Helpdesk Update](http://enviolo.qappture.com/enviolo-ProductRegistration-API-HelpdeskUpdate.jpg)

## Authors

* [Al Nolan](http://hustle.university)
* [Leah Nolan](https://www.linkedin.com/in/leahpnolan/)

## Acknowledgments

* qappture@gmail.com

## Support

* [Slack](http://qappture.slack.com)
* [Facebook](http://facebook.com/qappture)
* [Twitter](http://twitter.com/qappture)
* [Email](qappture@gmail.com)
