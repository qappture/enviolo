      <!--// Header - Guest [ //-->
      <header class="guest">

        <div class="view">

          <h1><?php echo $product_title; ?> Product Registration</h1>
          <span><i><?php echo $product_description; ?></i></span>
          <span><em><?php echo $product_disclaimer; ?></em></span>

        </div>

      </header>
      <!--// ] Header - Guest //-->
