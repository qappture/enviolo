  <!--// Head - Global [ //-->
  <head>

    <!--// Metadata //-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?php echo $product_page_title; ?>">

    <!--// Title //-->
    <title><?php echo $product_page_title; ?></title>

    <!--// Scripts //-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="bootstrap.js"></script>
    <script src="app.js"></script>
    <script src="typeahead.js"></script>

    <!--// Styles //-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" href="app.css">

    <!--// Favicons //-->
    <link rel="apple-touch-icon" sizes="57x57" href="http://www.enviolo.com/images/favicon/apple-icon-57x57.png"/>
    <link rel="apple-touch-icon" sizes="60x60" href="http://www.enviolo.com//images/favicon/apple-icon-60x60.png"/>
    <link rel="apple-touch-icon" sizes="72x72" href="http://www.enviolo.com//images/favicon/apple-icon-72x72.png"/>
    <link rel="apple-touch-icon" sizes="76x76" href="http://www.enviolo.com//images/favicon/apple-icon-76x76.png"/>
    <link rel="apple-touch-icon" sizes="114x114" href="http://www.enviolo.com//images/favicon/apple-icon-114x114.png"/>
    <link rel="apple-touch-icon" sizes="120x120" href="http://www.enviolo.com//images/favicon/apple-icon-120x120.png"/>
    <link rel="apple-touch-icon" sizes="144x144" href="http://www.enviolo.com//images/favicon/apple-icon-144x144.png"/>
    <link rel="apple-touch-icon" sizes="152x152" href="http://www.enviolo.com//images/favicon/apple-icon-152x152.png"/>
    <link rel="apple-touch-icon" sizes="180x180" href="http://www.enviolo.com//images/favicon/apple-icon-180x180.png"/>
    <link rel="icon" type="image/png" sizes="192x192" href="http://www.enviolo.com//images/favicon/android-icon-192x192.png"/>
    <link rel="icon" type="image/png" sizes="32x32" href="http://www.enviolo.com//images/favicon/favicon-32x32.png"/>
    <link rel="icon" type="image/png" sizes="96x96" href="http://www.enviolo.com//images/favicon/favicon-96x96.png"/>
    <link rel="icon" type="image/png" sizes="16x16" href="http://www.enviolo.com//images/favicon/favicon-16x16.png"/>
    <link rel="manifest" href="http://www.enviolo.com//images/favicon/manifest.json"/>
    <meta name="msapplication-TileColor" content="#ffffff"/>
    <meta name="msapplication-TileImage" content="http://www.enviolo.com//images/favicon/ms-icon-144x144.png"/>
    <meta name="theme-color" content="#ffffff"/>

  </head>
  <!--// ] Head - Global //-->
