$(document).ready(function() {

  jsonData();

  $('#myForm').submit(function (e) {

    var jData = $('#myForm');

    $.ajax({

      type: jData.attr('method'),
      url: jData.attr('action'),
      data: jData.serialize(),
      success: function(response) {
        jsonData();
        console.log(response);

      }

    });

    e.preventDefault();

  });

  function jsonData() {

    $('#output').empty();

    $.ajax({

      type: 'GET',
      url: 'json.php',
      success: function(response) {
        $.each(response, function(index) {

          //console.log(response[index].firstName);
          $('#output').append(

            response[index].firstName + ' ' +
            response[index].lastName + ' ' +
            response[index].age + '<br />'

          );

        });

        console.log(response);

      }

    });

  }

});
