<!DOCTYPE html>

<html lang="en">

  <head>

    <title>Enviolo - Product Registration</title>

    <meta charset="UTF-8">
    <meta name="description" content="Discoveryvip.com course tutorial">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <link rel="stylesheet" href="ux.css">

  </head>

  <body>

    <div id="container">

      Register your product(s): <br />

      <div id="oneForm">

        <form method="post" action="add.php" id="product_registration">

          <fieldset>
            <label>First Name</label>
            <input type="text" name="firstName" id="firstName">
          </fieldset>

          <fieldset>
            <label>Last Name</label>
            <input type="text" name="lastName">
          </fieldset>

          <fieldset>
            <label>Age</label>
            <input type="number" name="age">
          </fieldset>

          <fieldset>
            <input type="submit" id="buttonOne" value="add">
          </fieldset>

        </form>

      </div>

      <div id="output">
        Nothing Yet
      </div>

    </div>

    <script src="script.js"></script>

  </body>

</html>
