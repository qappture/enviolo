<!DOCTYPE html>

<html lang="en">

  <head>

    <title>PHP MYSQL JSON Discoveryvip</title>

    <meta charset="UTF-8">
    <meta name="description" content="Discoveryvip.com course tutorial">

    <link rel="stylesheet" href="style.css">

  </head>

  <body>

    <div id="container">

      Users: <br />

      <div id="oneForm">

        <form method="post" action="add.php" onsubmit="return submitData(this)">

          <fieldset>
            <label>First Name</label>
            <input type="text" name="firstName">
          </fieldset>

          <fieldset>
            <label>Last Name</label>
            <input type="text" name="lastName">
          </fieldset>

          <fieldset>
            <label>Age</label>
            <input type="number" name="age">
          </fieldset>

          <fieldset>
            <input type="submit" id="buttonOne" value="add">
          </fieldset>

        </form>

      </div>

    </div>

    <script src="script.js"></script>

  </body>

</html>
