<?php

  include 'conn.php';

  $sql = "SELECT * FROM ujson_users ORDER BY id DESC";
  $result = mysqli_query($conn, $sql) or die ("Error " . mysqli_error($conn));
  $myArray = array();

  //
  while($row = mysqli_fetch_assoc($result)) {

    $myArray[] = $row;

  }

  mysqli_close($conn);

  header('Content-Type: application/json');

  //$json = file_get_contents('json.json');

  /*
  $myArray = array(
    "user1" => array("firstName" => "Mike", "lastName" => "Smith", "age" => 34),
    "user2" => array("firstName" => "John", "lastName" => "Sams", "age" => 48)
  );
  */

  $json = json_encode($myArray);

  //$myArray = array("user1" => array("firstName" => "Mike2", "lastName" => "Smith" , "age" => 34),"user2" => array("firstName" => "Mike2", "lastName" => "Smith" , "age" => 34));

  //$json = json_encode($myArray);

  echo $json;

?>
