<?php

  //
  require 'app-api-connection.php';

/*ID*/$registration_ID = substr(md5(uniqid(microtime(true),true)),0,8);
/*partner_first_name*/if($_REQUEST['partner_first_name']){$partner_first_name = $_REQUEST['partner_first_name'];}else{$partner_first_name=NULL;}
/*partner_last_name*/if($_REQUEST['partner_last_name']){$partner_last_name = $_REQUEST['partner_last_name'];}else{$partner_last_name=NULL;}
/*partner_phone*/if($_REQUEST['partner_phone']){$partner_phone = $_REQUEST['partner_phone'];}else{$partner_phone=NULL;}
/*partner_address_postalcode*/if($_REQUEST['partner_address_postalcode']){$partner_address_postalcode = $_REQUEST['partner_address_postalcode'];}else{$partner_address_postalcode=NULL;}
/*partner_address_country*/if($_REQUEST['partner_address_country']){$partner_address_country = $_REQUEST['partner_address_country'];}else{$partner_address_country=NULL;}
/*partner_email*/if($_REQUEST['partner_email']){$partner_email = $_REQUEST['partner_email'];}else{$partner_email=NULL;}
/*partner_marketingoptin*/if($_REQUEST['partner_marketingoptin']){$partner_marketingoptin = $_REQUEST['partner_marketingoptin'];}else{$partner_marketingoptin=NULL;}
/*product_serialnumber*/if($_REQUEST['product_serialnumber']){$product_serialnumber = $_REQUEST['product_serialnumber'];}else{$product_serialnumber=NULL;}
/*product_brand*/if($_REQUEST['product_brand']){$product_brand = $_REQUEST['product_brand'];}else{$product_brand=NULL;}
/*product_dateofpurchase*/if($_REQUEST['product_dateofpurchase']){$product_dateofpurchase = $_REQUEST['product_dateofpurchase'];}else{$product_dateofpurchase=NULL;}
/*product_purchasedfrom_dealer*/if($_REQUEST['product_purchasedfrom_dealer']){$product_purchasedfrom_dealer = $_REQUEST['product_purchasedfrom_dealer'];}else{$product_purchasedfrom_dealer=NULL;}
/*product_purchasedfrom_dealercountry*/if($_REQUEST['product_purchasedfrom_dealercountry']){$product_purchasedfrom_dealercountry = $_REQUEST['product_purchasedfrom_dealercountry'];}else{$product_purchasedfrom_dealercountry=NULL;}
/*product_purchasedfrom_dealercitytown*/if($_REQUEST['product_purchasedfrom_dealercitytown']) { $product_purchasedfrom_dealercitytown = $_REQUEST['product_purchasedfrom_dealercitytown'];    }else{  $product_purchasedfrom_dealercitytown=NULL; }
/*product_awareness*/                   if($_REQUEST['product_awareness'])                    { $product_awareness =                    $_REQUEST['product_awareness'];                       }else{  $product_awareness=NULL;  }

  //
  if($_REQUEST) {

    $sql = $db->prepare(

      "INSERT INTO enviolo_registrations (

        registration_ID,
        registration_partner_first_name,
        registration_partner_last_name,
        registration_partner_phone,
        registration_partner_address_postalcode,
        registration_partner_address_country,
        registration_partner_email,
        registration_partner_marketingoptin,
        registration_product_serialnumber,
        registration_product_brand,
        registration_product_dateofpurchase,
        registration_product_purchasedfrom_dealer,
        registration_product_purchasedfrom_dealercountry,
        registration_product_purchasedfrom_dealercitytown,
        registration_product_awareness,
        registration_product_waranteedisclaimer

      ) VALUES (

        ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?

      )"

    );

    // Prepare binding...
    $sql->bind_param("sssssssissssssii",

      // ID data objects
      $registration_ID,
      $partner_first_name,
      $partner_last_name,
      $partner_phone,
      $partner_address_postalcode,
      $partner_address_country,
      $partner_email,
      $partner_marketingoptin,
      $product_serialnumber,
      $product_brand,
      $product_dateofpurchase,
      $product_purchasedfrom_dealer,
      $product_purchasedfrom_dealercountry,
      $product_purchasedfrom_dealercitytown,
      $product_awareness,
      $product_waranteedisclaimer

    );

    // Execution & closing...
    $sql->execute();
    $sql->close();

    $update = "The registration for {$partner_first_name} {$partner_last_name} ({$registration_ID}) has been successfully submitted.";

    echo $update;

  }

?>
